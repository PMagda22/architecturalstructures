#include "Church.h"
#include <sstream>

string Church::getReligion() const
{
	return this->m_religion;
}

void Church::setReligion(string religion)
{
	this->m_religion = religion;
}

string Church::getStyle() const
{
	return this->m_style;
}

void Church::setStyle(string style)
{
	this->m_style = style;
}

void Church::display(ostream& os) const
{
	os << "Church: ";
	ArchitecturalStructure::display(os);
	
	os << "," << m_religion << "," << m_style;
	os << endl;

}

istream& operator>>(istream& is, Church& c)
{
	
	int style;
	is >> c.m_name >> c.m_architect >> c.m_yearOfConstruction >> c.m_height >> c.m_country >> c.m_religion >> c.m_style;

	return is;
}
