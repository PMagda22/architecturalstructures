#include "Castle.h"
#include <sstream>

string Castle::getRoyal() const
{
	return m_royalty;
}

void Castle::setRoyal(string royal)
{
	this->m_royalty = royal;
}

void Castle::display(ostream& os) const
{
	os << "Castle: ";
	ArchitecturalStructure::display(os);
	os << "," << m_royalty;
	os << endl;
}


istream& operator>>(istream& is, Castle& c)
{
	
	is >> c.m_name >> c.m_architect >> c.m_yearOfConstruction >> c.m_height >> c.m_country >> c.m_royalty;
	return is;
	
}
