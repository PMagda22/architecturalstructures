#pragma once
#include <string>
#include <vector>
#include <istream>
#include <ostream>


using namespace std;


class ArchitecturalStructure
{
public:
	ArchitecturalStructure();                                                                               //contructor with no parameters
	ArchitecturalStructure(int type,string name, string architect, int year, int height,string country);    //constructor with parameters
	virtual ~ArchitecturalStructure() = default;                                                            //deconstructor

	void setType(int type);
	int getType() const;

	int getId() const;

	void setName(string name);
	string getName() const;

	void setArchitect(string architect);
	string getArchitect() const;

	void setYear(int year);
	int getYear() const;

	void setHeight(int height);
	int getHeight() const;

	void setCountry(string country);
	string getCountry() const;

	virtual void display(ostream& os) const;

	friend ostream& operator<<(ostream& os,const ArchitecturalStructure&);

	friend istream& operator>>(istream& is, ArchitecturalStructure&);

	friend bool operator==(const ArchitecturalStructure&, const ArchitecturalStructure&);     //verifies if 2 objects are equal

protected:
	int m_type;                         // 1 - churches    2 - castles
	string m_name;
	string m_architect;
	int m_yearOfConstruction;
	int m_height;
	string m_country;
	

private:
	int m_id;

	static int idGenerator;                 //we generate the id for an object created, we don't ask the user for id
};

