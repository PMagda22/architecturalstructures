#include "pch.h"
#include "CppUnitTest.h"
#include "../ArchitecturalStructures v2/ArchitecturalStructure.h"
#include "../ArchitecturalStructures v2/Repository.h"
#include "../ArchitecturalStructures v2/Controller.h"
#include "../ArchitecturalStructures v2/UI.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ArchitecturalStructuresTest
{
	TEST_CLASS(ArchitecturalStructuresTest)
	{
	public:
		
		TEST_METHOD(ConstructorArchitecturalStructure)
		{
			ArchitecturalStructure a;
			Assert::AreEqual(a.getType(), 0);
			string name = "no name";
			Assert::AreEqual(a.getName(), name);
			string architect = "no architect";
			Assert::AreEqual(a.getArchitect(), architect);
			Assert::AreEqual(a.getYear(), 0);
			Assert::AreEqual(a.getHeight(), 0);
			string country = "no country";
			Assert::AreEqual(a.getCountry(), country);

			name = "name";
			architect = "architect";
			country = "country";
			ArchitecturalStructure b{ 1,name,architect,1920,6,country };
			Assert::AreEqual(b.getType(), 1);
			Assert::AreEqual(b.getName(), name);
			Assert::AreEqual(b.getArchitect(), architect);
			Assert::AreEqual(b.getYear(), 1920);
			Assert::AreEqual(b.getHeight(), 6);
			Assert::AreEqual(b.getCountry(), country);

		}

		TEST_METHOD(GettersAndSetters)
		{
			string name = "name";
			string architect = "architect";
			string country = "country";
			ArchitecturalStructure b{ 1,name,architect,1920,6,country };
			Assert::AreEqual(b.getType(), 1);
			Assert::AreEqual(b.getName(), name);
			Assert::AreEqual(b.getArchitect(), architect);
			Assert::AreEqual(b.getYear(), 1920);
			Assert::AreEqual(b.getHeight(), 6);
			Assert::AreEqual(b.getCountry(), country);

			
			name = "name1";
			architect = "architect1";
			country = "country1";

			b.setType(2);
			b.setName(name);
			b.setArchitect(architect);
			b.setYear(2000);
			b.setHeight(7);
			b.setCountry(country);


			Assert::AreEqual(b.getType(), 2);
			Assert::AreEqual(b.getName(), name);
			Assert::AreEqual(b.getArchitect(), architect);
			Assert::AreEqual(b.getYear(), 2000);
			Assert::AreEqual(b.getHeight(), 7);
			Assert::AreEqual(b.getCountry(), country);
		}

		TEST_METHOD(AddArchitecturalStructure)
		{

			auto funct = [] {
				string type = "type";
				string name = "name";
				string architect = "architect";
				string year = "2000";
				string height = "5";
				string country = "country";
				Repository repo;
				Controller controller{ repo };
				controller.addController(type,name,architect,year,height,country); };
			Assert::ExpectException<exception>(funct);

			auto funct1 = [] {
				string type = "1";
				string name = "name";
				string architect = "architect";
				string year = "year";
				string height = "5";
				string country = "country";
				Repository repo;
				Controller controller{ repo };
				controller.addController(type, name, architect, year, height, country); };
			Assert::ExpectException<exception>(funct1);

			auto funct2 = [] {
				string type = "1";
				string name = "name";
				string architect = "architect";
				string year = "2000";
				string height = "height";
				string country = "country";
				Repository repo;
				Controller controller{ repo };
				controller.addController(type, name, architect, year, height, country); };
			Assert::ExpectException<exception>(funct2);


			Repository repo;
			Controller controller{ repo };

			Assert::AreEqual(repo.size(), 0);

			int type = 1;
			string name = "name";
			string architect = "architect";
			int year = 2000;
			int height = 5;
			string country = "country";
			ArchitecturalStructure* a = new ArchitecturalStructure;
			a->setType(type);
			a->setName(name);
			a->setArchitect(architect);
			a->setYear(year);
			a->setHeight(height);
			a->setCountry(country);
			repo.addRepo(a);
			Assert::AreEqual(repo.size(), 1);

		}

		TEST_METHOD(DeleteArchitecturalStructure)
		{
			auto funct = [] {
				string id = "eee";
				Repository repo;
				Controller controller{ repo };
				controller.deleteController(id); };
			Assert::ExpectException<exception>(funct);

			auto funct1 = [] {
				string id = "-1";
				Repository repo;
				Controller controller{ repo };
				controller.deleteController(id); };
			Assert::ExpectException<exception>(funct1);

			
			int id;
			Repository repo;
			Controller controller{ repo };

			int type = 1;
			string name = "name";
			string architect = "architect";
			int year = 2000;
			int height = 5;
			string country = "country";
			ArchitecturalStructure* a = new ArchitecturalStructure;
			a->setType(type);
			a->setName(name);
			a->setArchitect(architect);
			a->setYear(year);
			a->setHeight(height);
			a->setCountry(country);
			repo.addRepo(a);

			Assert::AreEqual(repo.size(), 1);
			id = a->getId();
			ArchitecturalStructure* as = repo.deleteRepo(id);
			Assert::IsNotNull(as);

			Assert::AreEqual(repo.size(), 0);
			
		}


		TEST_METHOD(UndoArchitecturalStructure)
		{
			auto funct = [] {
				Repository repo;
				Controller controller{ repo };
				controller.undoController(); };
			Assert::ExpectException<exception>(funct);


			Repository repo;
			Controller controller{ repo };

			string type = "1";
			string name = "name";
			string architect = "architect";
			string year = "2000";
			string height = "5";
			string country = "country";
			controller.addController(type, name, architect, year, height, country);
			

			controller.undoController();

			Assert::AreEqual(repo.size(), 0);

			try {
				controller.undoController();
				Assert::IsTrue(false);
			}
			catch (...)
			{
				Assert::IsTrue(true);
			}
		}

		TEST_METHOD(RedoArchitecturalStructure)
		{
			auto funct = [] {
				Repository repo;
				Controller controller{ repo };
				controller.redoController(); };
			Assert::ExpectException<exception>(funct);

			Repository repo;
			Controller controller{ repo };

			string type = "1";
			string name = "name";
			string architect = "architect";
			string year = "2000";
			string height = "5";
			string country = "country";
			controller.addController(type, name, architect, year, height, country);


			controller.undoController();

			controller.redoController();
			Assert::AreEqual(repo.size(), 0);

			try {
				controller.redoController();
				Assert::IsTrue(false);
			}
			catch (...)
			{
				Assert::IsTrue(true);
			}
		}
	};
}
